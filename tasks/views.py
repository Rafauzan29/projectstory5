from django.shortcuts import render, redirect
# from django.http import HttpResponse
from .forms import MatkulForm
from .models import Matkul
# Create your views here.
def index(request):

	form = MatkulForm()

	if request.method == "POST":
		form = MatkulForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect(index)

	context = {'form':form}
	return render(request, 'home.html', context)

def collections(request):
	matkul = Matkul.objects.all()
	context = {'matkul' : matkul}
	return render(request, 'collections.html', context)

def update(request, pk):
	matkul = Matkul.objects.get(id = pk)
	form = MatkulForm(instance = matkul)

	if request.method == "POST":
		form = MatkulForm(request.POST, instance = matkul)
		if form.is_valid():
			form.save()
		return redirect('coll')


	context = {'form':form}
	return render(request, 'updating.html', context)

def delete(request, pk):
	item = Matkul.objects.get(id = pk)


	if request.method == "POST":
		item.delete()
		return redirect('coll')


	context = {'item':item}
	return render(request, 'deleting.html', context)