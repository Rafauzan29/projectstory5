from django.db import models

# Create your models here.

class Matkul(models.Model):
	Nama_Mata_Kuliah = models.CharField(max_length = 200)
	Dosen_Pengajar = models.CharField(max_length = 200)
	Jumlah_SKS = models.IntegerField()
	Deskripsi_Mata_Kuliah = models.TextField()
	Semester_Tahun = models.CharField(max_length = 200)

	def __str__(self):
		return self.Nama_Mata_Kuliah